# Дополнительная монетизация
   
## Данный кусок кода выдернут из проекта написанного на базе Yii2

## Папка [additionalMonetization](https://gitlab.com/demini2/additional-monetization/-/tree/main/additionalMonetization?ref_type=heads)

#### Содержит базовый абстрактный сервис и два сервиса наследника, каждый из которых реализует родительские методы используя их для отправки данных в разные компании по API.

## Папка [commands](https://gitlab.com/demini2/additional-monetization/-/tree/main/commands?ref_type=heads)

#### Содержит саму команду, которая запускается по крону и отправляет данные лида, по очереди в разные компании, за отправку отвечают наследники класса [BaseApiService](https://gitlab.com/demini2/additional-monetization/-/blob/main/additionalMonetization/BaseApiService.php?ref_type=heads), используя методы базового класса.

## Папка [logs\TelegramBot](https://gitlab.com/demini2/additional-monetization/-/tree/main/logs/TelegramBot?ref_type=heads)

#### Содержит [класс](https://gitlab.com/demini2/additional-monetization/-/blob/main/logs/TelegramBot/TelegramBotLogs.php?ref_type=heads) отвечающий за отправку сообщений в телеграм бота.

## Папка [models](https://gitlab.com/demini2/additional-monetization/-/tree/main/models?ref_type=heads)

#### Содержит модели для взаимодействия с базой данных.
