<?php

namespace app\commands;

use app\logs\TelegramBot\TelegramBotLogs;
use app\additionalMonetization\AllianceApiService;
use app\additionalMonetization\BaseApiService;
use app\additionalMonetization\UnicoreApiService;
use app\models\Lead;
use Exception;

/**
 * Команда отправки лидов на доп монетизацию.
 */
class SendNotFullClientController
{
    /**
     * Экшен отправки лидов.
     * Задача: настроить отправку лидов на доп монетизацию,
     * лиды должны отправлять по очереди в 2 компании (в будущем количество увеличится).
     */
    public function actionIndex(): void
    {
        try {
            /** @var Lead[] $leads */
            $leads = (new Lead())->findAllLeadsWithClientNotFull();

            /** Если вернулся пустой массив заканчиваем команду */
            if (empty($leads)) {

                return;
            }

            /** Берем только четное число для корректной работы скрипта */
            if (0 !== count($leads) % 2) {
                var_dump(count($leads));
                return;
            }

            /** @var BaseApiService $apiServise */
            $apiServise = [
                0 => new UnicoreApiService(),
                1 => new AllianceApiService(),
            ];

            $count = 0;
            foreach ($leads as $lead) {

                /** Подготавливаем и отправляем данные партнеру */
                $result = $apiServise[$count]->sendData($lead);

                /** Записываем в базу результат отправки */
                $apiServise[$count]->createMonetizationLead($result);

                /** Устанавливаем лиду статус отправки на доп монетизацию */
                $lead->setStatusMonetizationLid(1);
                if (!$lead->save()) {
                    throw new Exception(message: "Ошибка обновления Lid Id: $lead->id ошибка: " . json_encode($lead->getFirstErrors()));
                }

                /** Проверяем счётчик и если он достиг нужного значения сбрасываем его */
                if (1 <= $count) {
                    $count = 0;
                    continue;
                }
                $count++;
            }

            TelegramBotLogs::sendMes(
                message: 'отправили ' . count($leads) . ' лида на доп монетизацию'
            );

            return;
        } catch (Exception $exception) {
            TelegramBotLogs::sendMes(
                message: "Ошибка отправки не полных лидов, на работу не влияет: {$exception->getMessage()}, {$exception->getFile()}, {$exception->getLine()} "
            );
        }
    }
}
