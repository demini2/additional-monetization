<?php

namespace app\logs\TelegramBot;

/**
 * Класс отправки сообщений в телеграм.
 */
class TelegramBotLogs
{
    /** @var int id чата группы */
    protected static int $chat_id = -1111;

    /** @var string Токен телеграм бота */
    protected static string $token = '';

    /**
     * Статичная функция отправки сообщения в телеграм
     *
     * @param string $message
     * @param int|null $chatId
     * @param $arrayQuery
     * @return void
     */
    public static function sendMes(string $message, int $chatId = null, $arrayQuery = null): void
    {
        $teleg = new static();
        $teleg->sendMessage($message, $chatId, $arrayQuery);
    }

    /**
     * Отправить сообщение ботом
     *
     * @param string $message
     * @param int|null $chatId
     * @param null $arrayQuery
     */
    public function sendMessage(string $message, int $chatId = null, $arrayQuery = null): void
    {
        if (null === $chatId) {
            $chatId = static::$chat_id;
        }
        if (null === $arrayQuery) {
            $arrayQuery = array(
                'chat_id' => $chatId,
                'text' => $message,
                'parse_mode' => "html",
            );
        }

        $this->send($arrayQuery);
    }

    /**
     * Для отправки текстовых сообщений
     *
     * @param array $arrayQuery
     * @return bool|string
     */
    protected function send(array $arrayQuery): bool|string
    {
        $ch = curl_init("https://api.telegram.org/bot" . static::$token . "/sendMessage?" . http_build_query($arrayQuery));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);

        return $res;
    }
}