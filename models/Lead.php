<?php

namespace app\models;

use DateTime;
use DateTimeZone;
use Exception;

/**
 * This is the model class for table "lead".
 *
 * @property int $id
 * @property int $channel_id Id рекламного канал
 * @property int|null $client_id Id клиента
 * @property int|null $client_not_full_id Id не полного клиента
 * @property string $creation_date Дата создания лида
 * @property int|null $status_api статус отправки по api
 * @property integer $status_monetization_lead статус отправки - 1 отправлено
 */
class Lead
{
    /**
     * Получить все лиды с не полными клиентами без статуса апи
     *
     * @return array
     * @throws Exception
     */
    public function findAllLeadsWithClientNotFull(): array
    {
        $data = new DateTime('now', new DateTimeZone('Europe/Moscow'));
        $oldData = $data->modify('-1 week')->format('Y-m-d H:i:s');

        return $this->findBySql(
            sql: 'SELECT * FROM ' . static::tableName() .
            ' WHERE `client_not_full_id` IS NOT NULL AND `status_api` IS NULL AND `creation_date`<:endDate AND `creation_date`>:startDate AND `status_monetization_lead` IS NULL LIMIT 4',
            params: [
                ':endDate' => $oldData,
                ':startDate' => '2024-03-29 00:00:00',
            ]
        )->all();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'lead';
    }

    /**
     * Установить значение полю status_monetization_lead
     *
     * @param int $statusMonetizationLid
     * @return void
     */
    public function setStatusMonetizationLid(int $statusMonetizationLid): void
    {
        $this->status_monetization_lead = $statusMonetizationLid;
    }
}
