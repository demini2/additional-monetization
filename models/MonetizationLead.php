<?php

namespace app\models;

/**
 * This is the model class for table "monetizationLead".
 *
 * @property int $id
 * @property int $lead_id id лида
 * @property string|null $source источник
 * @property string|null $where_sent куда отправили
 * @property string|null $status статус отправки
 * @property string|null $result результат отправки
 * @property string|null $date дата отправки
 * @property string|null $partner_lead_ld lead_ld у партнера
 * @property string|null $partner_lead_status статус лида у партнера'
 *
 * @property Lead $lid
 */
class MonetizationLead
{
    public const APPROVED = '1';
    public const CANCELLED = '2';

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'monetizationLead';
    }

    public function setLidId(int $lid_id): void
    {
        $this->lead_id = $lid_id;
    }

    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    public function setWhereSent(?string $where_sent): void
    {
        $this->where_sent = $where_sent;
    }

    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    public function setResult(?string $result): void
    {
        $this->result = $result;
    }

    public function setDate(?string $date): void
    {
        $this->date = $date;
    }

    public function setPartnerLeadLd(?string $partner_lead_ld): void
    {
        $this->partner_lead_ld = $partner_lead_ld;
    }

    public function setPartnerLeadStatus(?string $partner_lead_status): void
    {
        $this->partner_lead_status = $partner_lead_status;
    }
}
