<?php

namespace app\AdditionalMonetization;

use app\models\Lead;
use app\models\MonetizationLead;
use DateTimeImmutable;
use Exception;

/**
 * Сервис для отправки лидов на доп монетизацию в Unicore
 */
class UnicoreApiService extends BaseApiService
{
    /** @inheritDoc */
    protected string $token = '';

    /** @inheritDoc */
    protected string $url = 'https://';

    /** @inheritDoc */
    protected string $companyName = 'Unicore';

    /** @inheritDoc */
    protected Lead $lead;

    /**
     * @inheritDoc
     */
    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    /**
     * @inheritDoc
     */
    public function sendData(Lead $lead): bool|string|null
    {
        $this->lead = $lead;
        $data = $this->getData();

        return $this->send(
            curlOpt: [
                CURLOPT_URL => $this->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($data, flags: JSON_UNESCAPED_UNICODE),
                CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
            ]
        );
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function createMonetizationLead($result): void
    {
        $response = json_decode($result, associative: true);

        $partnerLeadLd = null;
        $partnerLeadStatus = MonetizationLead::CANCELLED;
        if (isset($response['lead_status']) && 'approved' === $response['lead_status']) {
            $partnerLeadStatus = MonetizationLead::APPROVED;
        }
        if (isset($response['lead_id'])) {
            $partnerLeadLd = (string)$response['lead_id'];
        }

        $monetization = new MonetizationLead();
        $monetization->setLidId($this->lead->id);
        $monetization->setSource($this->lead->sub1);
        $monetization->setWhereSent($this->getCompanyName());
        $monetization->setStatus(MonetizationLead::APPROVED);
        $monetization->setResult($result);
        $monetization->setPartnerLeadLd($partnerLeadLd);
        $monetization->setPartnerLeadStatus($partnerLeadStatus);
        $monetization->setDate((new DateTimeImmutable())->format(format: 'Y-m-d H:i:s'));
        if (!$monetization->save()) {
            throw new Exception(message: 'Ошибка сохранения MonetizationLid: ' . json_encode($monetization->getFirstErrors()));
        }
    }

    /**
     * @inheritDoc
     */
    protected function getData(): array
    {
        return [
            'phone' => '7' . $this->lead->clientNotFull->mobile_phone,
            'token' => $this->token,
            'campaign' => 'test',
            'external_id' => $this->lead->id,
            'sub1' => 'test',
            'first_name' => $this->lead->clientNotFull->first_name,
            'last_name' => $this->lead->clientNotFull->last_name,
            'father_name' => $this->lead->clientNotFull->patronymic,
        ];
    }
}
