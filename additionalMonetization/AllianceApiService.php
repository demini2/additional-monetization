<?php

namespace app\AdditionalMonetization;

use app\models\Lead;
use app\models\MonetizationLead;
use DateTimeImmutable;
use Exception;

/**
 * Сервис для отправки лидов на доп монетизацию в Alliance
 */
class AllianceApiService extends BaseApiService
{
    /** @inheritDoc */
    protected string $token = '';

    /** @inheritDoc */
    protected string $url = 'https://';

    /** @inheritDoc */
    protected string $companyName = 'Alliance';

    /** @inheritDoc */
    protected Lead $lead;

    /**
     * @inheritDoc
     */
    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    /**
     * @inheritDoc
     */
    public function sendData(Lead $lead): bool|string|null
    {
        $this->lead = $lead;
        $data = $this->getData();

        return $this->send($data);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function createMonetizationLead($result): void
    {
        $status = MonetizationLead::CANCELLED;
        if (-1 !== (int)$result) {
            $status = MonetizationLead::APPROVED;
        }

        $monetization = new MonetizationLead();
        $monetization->setLidId($this->lead->id);
        $monetization->setSource($this->lead->sub1);
        $monetization->setWhereSent($this->getCompanyName());
        $monetization->setStatus($status);
        $monetization->setResult(json_encode($result));
        $monetization->setPartnerLeadStatus($status);
        $monetization->setDate((new DateTimeImmutable())->format(format: 'Y-m-d H:i:s'));
        if (!$monetization->save()) {
            throw new Exception(message: 'Ошибка сохранения MonetizationLid: ' . json_encode($monetization->getFirstErrors()));
        }
    }

    /**
     * @inheritDoc
     */
    protected function getData(): array
    {
        $url = $this->url . '?phone=7' . $this->lead->clientNotFull->mobile_phone .
            '&token=' . $this->token .
            '&email=' . $this->lead->clientNotFull->email .
            '&name=' . $this->lead->clientNotFull->first_name .
            '&surname=' . $this->lead->clientNotFull->last_name .
            '&patronymic=' . $this->lead->clientNotFull->patronymic .
            '&date_birthday=' . $this->lead->clientNotFull->getBirthDateByForm('d.m.Y') .
            '&geo=' . $this->lead->clientNotFull->getCityName();

        return [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ];
    }
}
