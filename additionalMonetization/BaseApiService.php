<?php

namespace app\AdditionalMonetization;

use app\logs\TelegramBot\TelegramBotLogs;
use app\models\Lead;
use Exception;

/**
 * Базовый сервис для отправки лидов на доп монетизацию
 */
abstract class BaseApiService
{
    /** @var string Токен для авторизации у компании */
    protected string $token;

    /** @var string URL для отправки данных */
    protected string $url;

    /** @var string Название компании */
    protected string $companyName;

    /** @var Lead Лид по которому происходит отправка */
    protected Lead $lead;

    /**
     * Возвращает название компании
     */
    abstract public function getCompanyName(): string;

    /**
     * Отправить данные
     *
     * @param Lead $lead
     */
    abstract public function sendData(Lead $lead): bool|string|null;

    /**
     * Сохранить новую модель об отправке лида на доп монетизацию
     */
    abstract public function createMonetizationLead($result);

    /**
     * Подготовить данные для отправки
     */
    abstract protected function getData();

    /**
     * Метод отправки
     *
     * @param array $curlOpt
     * @return bool|string|null
     */
    protected function send(array $curlOpt): bool|string|null
    {
        try {
            $curl = curl_init();

            curl_setopt_array($curl, $curlOpt);

            $response = curl_exec($curl);

            curl_close($curl);

            return $response;
        } catch (Exception $exception) {
            TelegramBotLogs::sendMes(
                message: "Ошибка отправки в {$this->companyName}, на работу не влияет: {$exception->getMessage()}, {$exception->getFile()}, {$exception->getLine()} "
            );
        }

        return null;
    }
}
